import m from 'mithril'

import Dashboard from './modules/dashboard'

export default {
  view() {
    return m('.content-area', m(Dashboard))
  }
}