import m from 'mithril'

import './menu.css'

export default {
  view(vnode) {
    return m('ul.menu', vnode.children.map((child => {
      return m('li.menu-item', m('a', { href: child.href }, child.label))
    })))
  }
}
