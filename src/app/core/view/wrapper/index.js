import m from 'mithril'

import Header from '../header'
import Footer from '../footer'
import Container from '../container'
import Menu from '../menu'

import './wrapper.css'

// todo: find a better way for configuring the app menu
const menuItems = [
  {
    label: 'Home',
    href: '#!/',
  },
  {
    label: 'Product',
    href: '#!/product',
  },
  {
    label: 'Price',
    href: '#!/price',
  },
  {
    label: 'Contact',
    href: '#!/contact',
  },
]

export default {
  view(vnode) {

    return m('.wrapper',
      m('main.main', [
        Header,
        m(Menu, menuItems),
        m(Container, vnode.children),
        Footer,
      ]))
  }
}