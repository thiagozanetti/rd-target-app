import m from 'mithril'

export default {
  view() {
    return m('.dashboard', [
      m('h3.sub-title', 'Hello! welcome to the target app!'),
      m('p', 'This app\'s goal is to provide information for the rd-beholder library which tracks all the location changes and send it to the Node Gathering Service to make possible learn how users interact with sites / applications.')
    ])
  }
}