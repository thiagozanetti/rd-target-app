import m from 'mithril'

export default {
  view() {
    return m('.product', [
      m('h3.sub-title', 'Welcome to the product page.'),
      m('p', 'Here we can show how our product works, benefits and more.')
    ])
  }
}