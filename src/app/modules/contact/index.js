import m from 'mithril'

import './contact.css'

let submitted = false

const handleSubmit = () => submitted = true

export default {
  view() {
    return m('.contact', [
      m('h3.sub-title', 'We\'re willing to know you better!'),
      m('p', 'Please fill in the form so we can reach you and discuss more oportunities for you and your business.'),
      m('.form-container', m('form[method=post][action=javascript:void(0)].contact-form', { onsubmit: handleSubmit }, [
        m('label.contact-form__name__label', 'Name:'),
        m('input[type=text][required].contact-form__name__input.other'),
        m('label.contact-form__email__label', 'Email:'),
        m('input[type=email][required].contact-form__email__input'),
        (submitted) ? m('span', 'Thank you! We\'ll be in touch soon.') : '',
        m('button[type=submit]', 'Send')
      ]))
    ])
  }
}