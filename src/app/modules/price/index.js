import m from 'mithril'

export default {
  view() {
    return m('.price', [
      m('h3.sub-title', 'This is the price page.'),
      m('p', 'Where all the prices go.')
    ])
  }
}