import m from 'mithril'
import 'rd-beholder'

import routes from './app/core/routes'

import './index.css'

m.route(document.body, '/', routes)