# RD Target App

Chalenge toy app made with mithril.js :bow_and_arrow:

## How to use

First you have to install the dependencies. In your terminal:

```sh
npm install
```

then type 

```sh
npm start
``` 

And a new browser window will open on http://localhost:3000 after the application gets built.
